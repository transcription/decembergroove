#!/bin/bash

# IMPORT WTA FILES
# change file names before importing if it already exists in the target dir
# delete duplicates in the target directory

if [ $# -lt 1 ]
then
    echo "Usage: ./diff_schemas.sh external-schemas-directory"
    exit
fi

external_schemas_path=$1
external_schemas=`ls $external_schemas_path | egrep "\.wta"`

# import wta files
echo
for external in $external_schemas
do
# change file names before importing if it already exists in the target dir
    if [ `ls | egrep "\.wta" | egrep $external` ]
    then
        echo "$external already exists"
        external_renamed=`echo $external | cut -d'.' -f1`"_new-name.wta"
        external_wta_path=$external_schemas_path$external
        echo "imported as ./$external_renamed"
        cp $external_wta_path ./$external_renamed
        echo
    else
        external_wta_path=$external_schemas_path$external
        cp $external_wta_path .
        echo
    fi
done

# delete duplicates in the target directory
echo $'\n'"delete duplicates:"
fdupes -dN .

