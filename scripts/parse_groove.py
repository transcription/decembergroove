import sys
import logging
import re
import os
from pathlib import Path

# import music21 as m21
import pretty_midi
import verovio
import pandas as pd

# import numpy as np
import time
import librosa

import parsing

# global variables
_vtk = verovio.toolkit()
_dir_re = re.compile("D(\d+)S(\d)_(\d+)$")
_mid_re = re.compile("(\d+)_(\w+)(-\w+)*?_(\d+)_(\w*)_(\d+)-(\d+).mid$")
# default grammar file
_wta_file = str(Path('../schemas/drum-44.wta').absolute())
# default notation option
_notation = 'drum_agostini'
# use pyqparse module or standalone command line for transcription
_pyqparse = False

def check_midi(file):
    score = pretty_midi.PrettyMIDI(file)

    for instrument in score.instruments:
        notes = instrument.notes
        for i in range(len(notes) - 1):
            if (
                notes[i].pitch == notes[i + 1].pitch
                and notes[i].end == notes[i + 1].start
            ):
                notes[i].end -= 0.000001
    score.write(file)


def parse_file(file, outname, wta_file=None, notation=None):
    global _wta_file
    if wta_file == None:
        wta_file = _wta_file
    global _notation
    if notation == None:
        notation = _notation
    file_res = _mid_re.match(file)
    if file_res:
        _parse_file_res(file_res, outname, wta_file, notation)


def _parse_file_res(file_res, outname, wta_file, notation, log_flag):
    global _vtk
    global _pyqparse
    file = file_res.group(0)
    logging.info("File: " + file)
    logging.info(f"Check midi onsets and offsets for file {file}")
    check_midi(file)

    # get the duration of the recording
    dur = librosa.get_duration(filename=file.replace("mid", "wav"))

    (nb, style, s, tempo, beat, ts_num, ts_d) = file_res.groups()
    beat = beat == "beat"
    mei_name = outname + "_" + notation + "_test" + ".mei"
    logging.info(
        f"nb = {nb}; style = {style}; s = {s}; tempo = {tempo};\
                 beat = {beat}; ts_num = {ts_num}; ts_d = {ts_d}"
    )

    logging.info("Parsing to MEI file: " + mei_name)
    start_timer = time.time()

    if _pyqparse:
        parsing.parse_to_mei(mei_name, file, wta_file, True, notation, 
                             ts_num, ts_d, tempo, style)
    else:        
        #opt_verbose = ' -v 5'
        #opt_grammar = ' -a ' + wta_file
        #opt_midi    = ' -m ' + file 
        #opt_tempo   = ' -tempo ' + str(tempo)
        #opt_config  = ' -config ../params.ini'
        #opt_ts      = ' -ts ' + str(ts_num) + '/' + str(ts_d)
        #opt_out     = ' -o ' + mei_name
        cmd = '../drumparse' 
        cmd += ' -v 5'                                 # opt verbosity
        cmd += ' -a ' + wta_file                       # opt grammar file
        cmd += ' -m ' + file                           # opt midi file
        cmd += ' -tempo ' + str(tempo)                 # opt tempo 
        cmd += ' -config ../params.ini'                # opt config file
        cmd += ' -ts ' + str(ts_num) + '/' + str(ts_d) # opt timesig 
        cmd += ' -max -genre funk ' 
        cmd += ' -o ' + mei_name                       # opt out mei file        
        logging.info(cmd)
        os.system(cmd)                 

    end_timer = time.time() - start_timer
    logging.info("Parsing time:" + str(end_timer))

    # add the new parsing data
    if log_flag:
        add_parsing_data(outname, style, tempo, wta_file, end_timer, dur)

    ## load mei file with verovio and convert to svg
    svg_name = mei_name.replace(".mei", ".svg")
    logging.info("Creating:" + svg_name)
    _vtk = verovio.toolkit()
    _vtk.loadFile(mei_name)
    _vtk.renderToSVGFile(svg_name)

    ## svg to pdf with inkscape
    os.system("/usr/local/bin/inkscape --export-type='pdf' " + svg_name)


def parse_dir(folder, wta_file=None, notation=None):
    global _wta_file
    if wta_file == None:
        wta_file = _wta_file
    global _notation
    if notation == None:
        notation = _notation

    if not os.path.isdir(folder):
        logging.info("Directory not found: " + folder)
        return
    dir_res = _dir_re.match(folder)
    if dir_res == None:
        logging.info("Directory " + folder + " not processed")
        return
    print(f"\n************* Folder to process: {folder}")
    # perf_name = dir_res.group(0)
    logging.info("\nDirectory : " + dir_res.group(0))

    # search and process the midi file in dir
    midifound = False
    for file in os.listdir(folder):
        file_res = _mid_re.match(file)
        if file_res == None:
            continue
        else:
            midifound = True
            break
    if not midifound:
        logging.info("No MIDI file found in: " + dir_res.group(0))
        return

    os.chdir(folder) 
    outname = dir_res.group(0)                           # log flag
    _parse_file_res(file_res, outname, wta_file, notation, False)
    os.chdir('..')

def parse_all(wta_file=None, notation=None):
    global _wta_file
    if wta_file == None:
        wta_file = _wta_file
    global _notation
    if notation == None:
        notation = _notation

    # directory = os.getcwd()  # current working dir
    print(f"launching directory: {os.getcwd()}")
    directory = os.chdir("..")
    print(f"process directory: {os.getcwd()}")
    for folder in os.listdir(directory):
        parse_dir(folder, wta_file, notation)


def add_parsing_data(performance, style, tempo, grammar, parsing_time, dur):
    parsing_data["performance"].append(performance)
    parsing_data["style"].append(style)
    parsing_data["tempo"].append(tempo)
    parsing_data["grammar"].append(grammar)
    parsing_data["parsing_time"].append(parsing_time)
    parsing_data["wav_duration"].append(dur)


def main():
    # set_verbosity_level(1)
    if len(sys.argv) > 1:
        if ".wta" in sys.argv[1]:
            wta_file = sys.argv[1]
        else:
            logging.error("The argument must be a WTA file : " + sys.argv[1])
    else:
        global _wta_file
        wta_file = _wta_file # default grammar
        logging.warning("No schema file provided. Using default:" + wta_file)    

    parse_all(wta_file, "drum_agostini")
    parse_all(wta_file, "drum_us")
    df = pd.DataFrame(parsing_data)
    print(df)


if __name__ == "__main__":
    parsing_data = {
        "performance": [],
        "style": [],
        "tempo": [],
        "grammar": [],
        "parsing_time": [],
        "wav_duration": [],
    }

    logging.basicConfig(
        format="[PYTHON LOGGER %(levelname)s] %(message)s", level=logging.DEBUG
    )
    main()
