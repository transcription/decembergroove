# qparse python binding for drum transcription `pyqparse.cpython-v-platform.so`

## requirements 
- python binding for transcription `pyqparse.cpython-v-platform.so`
  where `v` is the Python version
  compiled with https://gitlab.inria.fr/qparse/qparselib, branch `gmd-score`
  in dir `scripts`
  can be a symlink
- file `parsing.py` of   
  https://gitlab.inria.fr/lyrodrig/pyqparse, dir `qparse/`
  copy or symlink

## content

- `parse_groove.py`
mod. for this repo of the file of the same name from https://gitlab.inria.fr/lyrodrig/pyqparse, dir `tests/groove_transcription/`

