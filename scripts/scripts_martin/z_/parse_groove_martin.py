import sys
import logging
import re
import os
import pretty_midi
import verovio
import time
import parsing_martin

# import librosa


# GLOBAL VARIABLES
_vtk = verovio.toolkit()
_mid_re = re.compile(r"(\d+)_(\w+)(-\w+)*?_(\d+)_(\w*)_(\d+)-(\d+).mid$")
_pyqparse = False  # pyqparse module or standalone cmd line for transcription


def check_midi(file):
    score = pretty_midi.PrettyMIDI(file)
    for instrument in score.instruments:
        notes = instrument.notes
        for i in range(len(notes) - 1):
            if (
                notes[i].pitch == notes[i + 1].pitch
                and notes[i].end == notes[i + 1].start
            ):
                notes[i].end -= 0.000001
    score.write(file)


def _parse_file_res(file_res, outname, wta_file, notation):
    global _vtk
    global _pyqparse
    file = file_res.group(0)
    logging.info(file)
    check_midi(file)
    #     dur = librosa.get_duration(filename=file.replace("mid", "wav"))
    (nb, style, _, tempo, beat, ts_num, ts_d) = file_res.groups()
    beat = beat == "beat"
    mei_name = outname + "_parse_groove_martin_output.mei"
    root = "/".join(os.getcwd().split("/")[:-1]) + "/"
    local_mei_path = os.getcwd().split("/")[-1]
    mei_name = f"{root}OUTPUTs_MARTIN/{local_mei_path}/{mei_name}"
    logging.info(
        f"file: {nb}, style: {style}, tempo: {tempo}, " f"ts: {ts_num}/{ts_d}"
    )
    logging.info("Parsing to MEI file: " + mei_name)
    start_timer = time.time()
    if _pyqparse:
        parsing_martin.parse_to_mei(
            mei_name,
            file,
            wta_file,
            True,
            notation,
            ts_num,
            ts_d,
            tempo,
            style,
        )
    else:
        cmd = "/home/martin/ADT/qparselib/build/drumparse"
        cmd += " -v 0"
        cmd += " -a " + wta_file
        cmd += " -m " + file
        cmd += " -tempo " + str(tempo)
        cmd += " -config /home/martin/ADT/decembergroove/params.ini"
        cmd += " -ts " + str(ts_num) + "/" + str(ts_d)
        cmd += " -max -genre " + style
        cmd += " -o " + mei_name
        log_cmd = cmd.replace(" -", "\n-")
        logging.info(f"\n{log_cmd}")
        os.system(cmd)

    end_timer = time.time() - start_timer
    logging.info("Parsing time:" + str(end_timer))

    ## load mei file with verovio and convert to svg
    svg_name = mei_name.replace(".mei", ".svg")
    logging.info("Creating:" + svg_name)
    _vtk = verovio.toolkit()
    _vtk.loadFile(mei_name)
    _vtk.renderToSVGFile(svg_name)

    ## svg to pdf with inkscape
    os.system("/usr/bin/inkscape --export-type='pdf' " + svg_name)


def add_parsing_data(performance, style, tempo, grammar, parsing_time, dur):
    parsing_data["performance"].append(performance)
    parsing_data["style"].append(style)
    parsing_data["tempo"].append(tempo)
    parsing_data["grammar"].append(grammar)
    parsing_data["parsing_time"].append(parsing_time)
    parsing_data["wav_duration"].append(dur)


def parse_dir(folder, wta_file, notation):
    outname = ""
    file_res = _mid_re.match

    if not os.path.isdir(folder):
        logging.info("Directory not found: " + folder)
        return
    logging.info(folder)

    midifound = False
    for file in os.listdir(folder):
        file_res = _mid_re.match(file)
        if file_res == None:
            continue
        else:
            midifound = True
            outname = file.split(".")[0]
            break
    if not midifound:
        logging.info("No MIDI file found in: " + folder)
        return

    os.chdir(folder)
    _parse_file_res(file_res, outname, wta_file, notation)
    os.chdir("..")


def parse_all(dataset, wta_file, notation):
    os.chdir(dataset)
    for dir in os.listdir("."):
        if dir.endswith("OUTPUTs_MARTIN"):
            continue
        print()
        parse_dir(dir, wta_file, notation)
    os.chdir("..")


def main():
    if len(sys.argv) < 4:
        print(
            "Usage: python parse_groove.py "
            "--all-dataset|--directory <dataset>|<directory> <wta file>"
        )
    else:
        wta_file = sys.argv[3]
        if sys.argv[1] == "--directory":
            parse_dir(sys.argv[2], wta_file, "drum_us")
        elif sys.argv[1] == "--all-dataset":
            print(f"\nPARSE ALL DATASET: {sys.argv[2]}")
            print(
                "OUTPUT DIRECTORY: decembergroove/drum_scores/OUTPUTs_MARTIN"
            )
            parse_all(sys.argv[2], wta_file, "drum_us")
    print()


if __name__ == "__main__":
    parsing_data = {
        "performance": [],
        "style": [],
        "tempo": [],
        "grammar": [],
        "parsing_time": [],
        "wav_duration": [],
    }
    logging.basicConfig(
        format="[PYTHON LOGGER %(levelname)s] %(message)s", level=logging.DEBUG
    )
    main()
