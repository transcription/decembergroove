"""
Test :
python3 parsing.py 
"""

import sys
import logging

# import re
# import verovio
# import pretty_midi
# import os
# from pathlib import Path
# import music21 as m21

sys.path.insert(0, "/home/martin/ADT/qparselib/build")
from pyqparse import *


def parse_midi_file(
    file_name,
    wta_filepath,
    max=True,
    type="mono",
    ts_num=0,
    ts_d=0,
    tempo=0,
    style="funk",
    beats_file="",
    wcode=WeightCode.Tropical,
    clef={},
):
    """Parses a midi file to a score model.

    Args:
        file_name: A string of the path to the MIDI file.
        wta_filepath: A string of the path to the grammar file.
        max: If True, the runs are enumerated from best to worst,
            if False, from worst to best.
        type: A string corresponding to the type of score,
            this sets the parsing model.
        ts_num: The numerator of the time signature.
        ts_d: The denominator of the time signature.
        tempo: Tempo of the MIDI file, if known.
        style: For drums only: a string which will set the engraving style according.
        beats_file: A string of the path to a file containing
            the timestamps of the beats of the MIDI file.
        wcode: The weight type to used for parsing.

    Returns:
        A instance of a ScoringEnv containing the resulting Score Model.

    """
    assert isinstance(ts_num, int)
    assert isinstance(ts_d, int)
    assert isinstance(tempo, int)

    set_config(1)

    # set_verbosity_level(5)

    if type == "mono":
        mode = Mode.Mono
        pmodel = Model.MonoLR
        logging.info(
            "In mono mode; mode = Mode.Mono ; parsing model = Model.MonoLR"
        )
    elif type == "drum_agostini":
        mode = Mode.Drum
        pmodel = Model.DrumAgostini
        logging.info(
            "In agostini drum mode; mode = Mode.Drum ; parsing model = Model.DrumAgostini"
        )
    elif type == "drum_us":
        mode = Mode.Drum
        pmodel = Model.DrumUS
        logging.info(
            "In US drum mode; mode = Mode.Drum ; parsing model = Model.DrumUS"
        )
    elif type == "plain":
        mode = Mode.Plain
        # pmodel = Model.DrumAgostini
        logging.error("TODO")
        return
    else:
        logging.error("type doesn't exist")
        return

    assert mode

    if beats_file:
        tempo_model = Tempo.Beats
    else:
        tempo_model = Tempo.Change

    def parse():
        # create an environment
        env = ScoringEnv()

        # set on debug mode
        env.set_debug(False)

        # choose ordering for Parsing
        env.set_order(not max)

        # set rewrite rules
        env.set_trs(mode)

        # import input segment
        # tracknb = 1
        status = env.import_file(file_name, Format.MIDI, mode)
        if status != 0:
            logging.error("Error while importing file : " + str(status))

        # import changes
        status = env.import_changes(file_name)
        if status > 0:
            logging.error(
                f"failed to import tempo info from MIDI file {file_name}, error {status}"
            )

        # add voicing
        if "drum" in type:
            env.create_voicing_drum(style)
        else:
            env.create_voicing(mode)
        env.revoice()

        # time sig
        if ts_num and ts_d:
            # env.set_time_sig(ts_num, ts_d, True)
            env.update_time_sig(ts_num, ts_d)
        else:
            env.update_time_sig()

        # import beats
        if beats_file:
            env.import_beats(beats_file)
            env.cut_segment_beats()
            env.set_tempo(Tempo.Beats, 0)
        elif tempo:
            barbeats = env.get_bar_beats()
            barsec = (60 / tempo) * barbeats

            logging.info(f"barsec : {barsec}")
            logging.info(f"barbeats : {barbeats}")

            env.set_tempo(Tempo.Constant, barsec)
        elif env.has_changes():
            status = env.set_tempo(tempo_model)
            if status > 0:
                logging.error(
                    f"fail initializing tempo with tempo change info, \
                   abort (error {status})"
                )
        else:
            logging.error("There should be either the tempo or a beats_file")

        # logging.info(f"initial tempo = {env.tempo().bpm()}bpm ({env.tempo().spb()}s per bar)")

        # import WTA schema
        # wcode = WeightCode.Tropical
        env.set_weight(wcode)
        status = env.import_WTA(wta_filepath, wcode)
        assert status == 0

        # prepare parsing
        env.set_open(True)

        # open score
        env.open_score(file_name)

        # add clef
        if clef:
            env.add_clef(clef["sign"], clef["line"], clef["octave"])
            logging.info(
                f"Clef added : sign: {clef['sign']}, line: {clef['line']}, octave: {clef['octave']}"
            )

        # parse
        env.add_part("part1", pmodel, 1)

        return env

    return parse()


def parse_to_m21(
    midi_name,
    wta_filepath,
    type,
    tempo,
    style,
    ts_num=4,
    ts_d=4,
    max=True,
    beats_file="",
):
    logging.info(f"Parsing to music21 the file {midi_name}")

    env = parse_midi_file(
        midi_name,
        wta_filepath,
        max,
        type,
        int(ts_num),
        int(ts_d),
        int(tempo),
        style,
        beats_file,
    )

    return from_score_model(env, False)


def get_nb_measures(env):
    logging.info(type(env))
    return nb_measures(env)


def parse_to_mei(
    mei_name,
    midi_name,
    wta_filepath,
    max,
    type,
    ts_num,
    ts_d,
    tempo,
    style,
    beats_file="",
):
    logging.info(f"Parsing to music21 the file {midi_name}")

    env = parse_midi_file(
        midi_name,
        wta_filepath,
        max,
        type,
        int(ts_num),
        int(ts_d),
        int(tempo),
        style,
        beats_file,
    )
    # print_score_model(env)

    logging.info(f"Exporting score model to mei in {mei_name}")
    status = env.export_MEI(mei_name)

    if status != 0:
        logging.error("Exportation to MEI failed.")
        return

    # env.close_score()
    return env


# def main():
#     # set_verbosity_level(1)
#     if len(sys.argv) > 1:
#         if ".wta" in sys.argv[1]:
#             wta_filepath = sys.argv[1]
#         else: logging.error("The argument must be a WTA file : " + sys.argv[1])
#     else:
#         wta_filepath = "../schemas/drum-44.wta"
#         logging.warning("No schema file provided. By default, the schema used is : " + wta_filepath)


#     parse_dataset("../", wta_filepath, "drum_agostini")
#     parse_dataset("../", wta_filepath, "drum_us")


# if __name__ == "__main__":
#     tk = verovio.toolkit()

#     logging.basicConfig(format='[PYTHON LOGGER %(levelname)s] %(message)s', level=logging.DEBUG)
#     main()
