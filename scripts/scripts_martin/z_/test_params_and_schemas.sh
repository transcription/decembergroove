#!/bin/bash

# ARGUMENTS ###################################################################
error_and_exit() {
    echo "Usage: test_params_and_schemas.sh"\
         "[target directory] [params directory] [schemas directory]"$'\n'
    exit
}
if [ $# -lt 3 ]
then
    echo $'\n''ARGUMENTS ARE MISSING'
    error_and_exit
elif [ $# -gt 3 ]
then
    echo $'\n''TOO MANY ARGUMENTS'
    error_and_exit
fi
target_dir=`realpath $1`"/"
params_dir=`realpath $2`"/"
schemas_dir=`realpath $3`"/"
for arg in $target_dir $params_dir $schemas_dir
do
    if ! [ -d $arg ]
    then
        echo $'\n'"TARGET DIRECTORY ERROR"
        echo "$arg is not a directory"
        error_and_exit
    fi
done
###############################################################################

echo
echo "[PROCESS DIRECTORY] $target_dir"$'\n'
for p in `ls $params_dir | egrep "\.ini"`
do
    params_file=$params_dir$p
    for s in `ls $schemas_dir | egrep "\.wta"`
    do
        schema_file=$schemas_dir$s
        echo
        echo "----------------------------------------------------------------"
        echo "py drumparse_to_pdf.py --one-file "
        echo "$target_dir"
        echo "$params_file"
        echo "$schema_file"
        echo "----------------------------------------------------------------"
        py drumparse_to_pdf.py --one-file $target_dir $params_file $schema_file
        echo
    done
done
