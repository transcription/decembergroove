import os
import sys
import verovio


def input_error():
    print(
        "Usage: py drumparse_to_pdf.py "
        "[--dataset |--one-file] "
        "[directory] "
        "[ini file] "
        "[wta file] "
    )
    sys.exit()


def check_args():
    if len(sys.argv) < 5:
        input_error()
    option = sys.argv[1]
    target = sys.argv[2]
    params = sys.argv[3]
    schema = sys.argv[4]
    if not option == "--dataset" and not option == "--one-file":
        print("OPTION ERROR")
        input_error()
    if not os.path.isdir(target):
        print("TARGET DIRECTORY ERROR")
        input_error()
    if not params.endswith(".ini"):
        print("INI FILE ERROR")
        input_error()
    params = sys.argv[3]
    if not schema.endswith(".wta"):
        print("WTA FILE ERROR")
        input_error()
    return option, target, params, schema


def drum_parse(midi_file_path, tempo, ts, genre):
    print(f"INI FILE: {params}\nWTA FILE: {schema}")
    ini_name = params.split("/")[-1]
    wta_name = schema.split("/")[-1]
    output_file_added_name = f"__{ini_name}__{wta_name}__"
    mei_dataset_path = midi_file_path.replace(
        ".mid", f"_drumparse_to_pdf{output_file_added_name}output.mei"
    )
    mei_file_path = "/".join(mei_dataset_path.split("/")[-2:])
    print(mei_file_path)
    mei_dataset_path = mei_dataset_path.replace(
        mei_file_path, f"OUTPUTs_MARTIN/{mei_file_path}"
    )
    print(mei_dataset_path)
    os.system(
        f"/home/martin/ADT/qparselib/build/drumparse "
        f"-a {schema} "
        f"-m {midi_file_path} "
        f"-tempo {tempo} "
        f"-config {params} "
        f"-ts {ts} "
        f"-max "
        f"-genre {genre} "
        f"-o {mei_dataset_path} "
    )
    return mei_dataset_path


def mei_to_pdf(mei_path):
    print(mei_path)
    tk = verovio.toolkit()
    tk.loadFile(mei_path)
    svg_path = mei_path.replace(".mei", ".svg")
    tk.renderToSVGFile(svg_path)
    os.system("inkscape --export-type='pdf' " + svg_path)


def parse_dir(dir_path):
    print(f"\nDIRECTORY TO PROCESS: {dir_path}")
    midi_file = "".join(
        [file for file in os.listdir(dir_path) if file.endswith(".mid")]
    )
    midi_file_path = dir_path + midi_file
    print(f"MIDI FILE TO PROCESS: {midi_file_path}")
    midi_file_for_args = midi_file.split(".")[0]
    _, genre, tempo, _, ts = midi_file_for_args.split("_")
    ts = ts.replace("-", "/")
    genre = genre.split("-")[0]
    mei_dataset_path = drum_parse(midi_file_path, tempo, ts, genre)
    print(mei_dataset_path)
    mei_to_pdf(mei_dataset_path)


def parse_dataset(dataset):
    print(f"\nPARSE ALL DATASET: {dataset}")
    print("OUTPUT DIRECTORY: decembergroove/drum_scores/OUTPUTs_MARTIN")
    for dir in os.listdir(dataset):
        dir_path = f"{dataset}{dir}/"
        if dir_path.endswith("OUTPUTs_MARTIN/"):
            continue
        parse_dir(dir_path)
    print()


def main():
    if sys.argv[1] == "-all":
        parse_dataset(sys.argv[2])
    elif sys.argv[1] == "-dir":
        parse_dir(sys.argv[2])


if __name__ == "__main__":
    option, target, params, schema = check_args()
    if option == "--one-file":
        parse_dir(target)
    elif option == "--dataset":
        parse_dataset(target)
    else:
        print("OPTION ERROR")
        sys.exit()
