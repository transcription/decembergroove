import os
import sys
from music21 import mei


def args():
    if len(sys.argv) < 2:
        print("Usage: python test_mei_to_lilypond mei_input")
        sys.exit()
    f_list = os.listdir(sys.argv[1])
    f_name = "".join([f for f in f_list if f.endswith(".mei")])
    mei_path = f"{sys.argv[1]}{f_name}"
    return mei_path


def main():
    pass


if __name__ == "__main__":
    mei_file = args()
    with open(mei_file) as mf:
        mf = mf.read()

    print(mf)

#     mei.MeiToM21Converter(mei_file)
