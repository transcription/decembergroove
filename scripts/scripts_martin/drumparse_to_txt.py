import os
import sys
import verovio
import subprocess


def input_error():
    print(
        "Usage: python drumparse_to_txt.py "
        "[--dataset |--one-file] "
        "[directory] "
        "[ini file] "
        "[wta file] "
    )
    sys.exit()


def check_args():
    if len(sys.argv) < 5:
        input_error()
    option = sys.argv[1]
    target = sys.argv[2]
    params = sys.argv[3]
    schema = sys.argv[4]
    if not option == "--dataset" and not option == "--one-file":
        print("OPTION ERROR")
        input_error()
    if not os.path.isdir(target):
        print("TARGET DIRECTORY ERROR")
        input_error()
    if not params.endswith(".ini"):
        print("INI FILE ERROR")
        input_error()
    params = sys.argv[3]
    if not schema.endswith(".wta"):
        print("WTA FILE ERROR")
        input_error()
    return option, target, params, schema


def drum_parse(midi_file_path, tempo, ts, genre):
    print(f"INI FILE: {params}\nWTA FILE: {schema}")
    ini_name = params.split("/")[-1]
    wta_name = schema.split("/")[-1]
    output_file_added_name = f"__{ini_name}__{wta_name}__"
    mei_dataset_path = midi_file_path.replace(
        ".mid", f"_drumparse_to_pdf{output_file_added_name}output.mei"
    )
    mei_file_path = "/".join(mei_dataset_path.split("/")[-2:])
    mei_dataset_path = mei_dataset_path.replace(
        mei_file_path, f"OUTPUTs_MARTIN/{mei_file_path}"
    )
    txt_dataset_path = mei_dataset_path.replace(".mei", ".txt")
    print(f">>>> {txt_dataset_path}")
    output_file = open(txt_dataset_path, "a")
    txt_output = subprocess.Popen(
        [
            f"/home/martin/ADT/qparselib/build/drumparse ",
            f"-v 0 ",
            f"-m {midi_file_path} ",
            f"-a {schema} ",
            f"-tempo {tempo} ",
            f"-ts {ts} ",
            f"-config {params} ",
            f"-max ",
            f"-genre {genre} ",
        ],
        stdout=output_file,
        stderr=output_file,
        shell=True,
    )
    print(txt_output)
    return mei_dataset_path


def mei_to_pdf(mei_path):
    print(mei_path)
    tk = verovio.toolkit()
    tk.loadFile(mei_path)
    svg_path = mei_path.replace(".mei", ".svg")
    tk.renderToSVGFile(svg_path)
    os.system("inkscape --export-type='pdf' " + svg_path)


def parse_dir(dir_path):
    print(f"\nDIRECTORY TO PROCESS: {dir_path}")
    midi_file = "".join(
        [file for file in os.listdir(dir_path) if file.endswith(".mid")]
    )
    midi_file_path = dir_path + midi_file
    print(f"MIDI FILE TO PROCESS: {midi_file_path}")
    midi_file_for_args = midi_file.split(".")[0]
    _, genre, tempo, _, ts = midi_file_for_args.split("_")
    ts = ts.replace("-", "/")
    genre = genre.split("-")[0]
    drum_parse(midi_file_path, tempo, ts, genre)
    # mei_dataset_path = drum_parse(midi_file_path, tempo, ts, genre)
    # mei_to_pdf(mei_dataset_path)


def parse_dataset(dataset):
    print(f"\nPARSE ALL DATASET: {dataset}")
    print("OUTPUT DIRECTORY: decembergroove/drum_scores/OUTPUTs_MARTIN")
    for dir in os.listdir(dataset):
        dir_path = f"{dataset}{dir}/"
        if dir_path.endswith("OUTPUTs_MARTIN/"):
            continue
        parse_dir(dir_path)
    print()


def main():
    if sys.argv[1] == "-all":
        parse_dataset(sys.argv[2])
    elif sys.argv[1] == "-dir":
        parse_dir(sys.argv[2])


if __name__ == "__main__":
    option, target, params, schema = check_args()
    if option == "--one-file":
        parse_dir(target)
    elif option == "--dataset":
        parse_dataset(target)
    else:
        print("OPTION ERROR")
        sys.exit()


###############################################################################


# sys.exit()
# os.system(
#     f"/home/martin/ADT/qparselib/build/drumparse "
#     f"-v 0 "
#     f"-m {midi_file_path} "
#     f"-a {schema} "
#     f"-tempo {tempo} "
#     f"-ts {ts} "
#     f"-config {params} "
#     f"-max "
#     f"-genre {genre} "
#     # f"-o {mei_dataset_path} "
#     f"> {txt_dataset_path}"
# )
# c = subprocess.Popen(
# "/home/martin/ADT/qparselib/build/drumparse",
# stdout=log,
# stderr=log,
# shell=True,
# )
# f"-o {mei_dataset_path} "
# f"> {txt_dataset_path}"
