#!/bin/bash

# ARGUMENTS ###################################################################
error_and_exit() {
    echo "Usage: launch_drumparse-to-pdf.sh [option: --one-file|--dataset]"\
         "[target directory] [params directory] [schemas directory]"$'\n'
    exit
}
if [ $# -lt 4 ]
then
    echo $'\n''ARGUMENTS ARE MISSING'
    error_and_exit
elif [ $# -gt 4 ]
then
    echo $'\n''TOO MANY ARGUMENTS'
    error_and_exit
fi
option=$1
target_dir=`realpath $2`"/"
params_dir=`realpath $3`"/"
schemas_dir=`realpath $4`"/"
if ! [ $option == "--one-file" ] && ! [ $option == "--dataset" ]
then
    echo $'\n'"$option >>> OPTION ERROR"
    error_and_exit
fi
for arg in $target_dir $params_dir $schemas_dir
do
    if ! [ -d $arg ]
    then
        echo $'\n'"TARGET DIRECTORY ERROR"
        echo "$arg is not a directory"
        error_and_exit
    fi
done
###############################################################################

echo
echo "[PROCESS DIRECTORY] $target_dir"$'\n'
for p in `ls $params_dir | grep "\.ini"`
do
    params_file=$params_dir$p
    for s in `ls $schemas_dir | grep "\.wta"`
    do
        schema_file=$schemas_dir$s
        echo
        echo "----------------------------------------------------------------"
        echo "python drumparse_to_pdf.py $option "
        echo "$target_dir"
        echo "$params_file"
        echo "$schema_file"
        echo "----------------------------------------------------------------"
        python drumparse_to_pdf.py $option $target_dir $params_file $schema_file
        echo
    done
done
