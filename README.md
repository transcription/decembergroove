# DecemberGroove
MIDI-to-score transcription experiments on the MIDI files of the 
Groove MIDI dataset v.1.0.0 midionly
- https://magenta.tensorflow.org/datasets/groove

## requirements for transcription
- command line `drumparse` for transcription
  compiled with `https://gitlab.inria.fr/qparse/qparselib`, branch `gmd-score`
  in root dir
  can be a symlink
- python binding for transcription `pyqparse.cpython-v-platform.so`
  where `v` is the Python version
  compiled with `https://gitlab.inria.fr/qparse/qparselib`, branch `gmd-score`
  in dir `scripts`
  can be a symlink
- see `scripts/README.md` for other requirements


## content

- 1 directory `DiSj_n` corresponds to the GMD entry of:  
  - drummer `i`  
  - session `j`  
  - MIDI file `n`  

every dir `DiSj_n` should contain exactly 
- one MIDI file of name `n_genre_tempo_beatorfill_ts.mid`, where
  - `n` is the same number as the above `n` in directory name
  - `genre` is in `groove`, `fast`, `mediumfast`, `brazilian`, `brazilianbaiao`, `funk`, `halftime`, `purdieshuffle`, `samba`, `prog`, `bembe`, `rhumba`, `rock`, `braziliansambareggae`, `brazilianmaracatu`, `fusion`, `disco`, `shuffle`, `reggaeton`, `breakbeat`, `chacarera`, `folk`, `calypso`, `swing`, `rockabilly`, `linear`
  - `tempo` in bpm
  - `beatorfill` = `beat` or `fill`
  - `ts` = time signature, in general `4-4`
- `DiSj_n_drum_notation.mei` the [MEI](https://music-encoding.org) output of [qparse](https://qparse.gitlabpages.inria.fr)
   where `notation` is either `agostini` or `us`
- `DiSj_n_drum_notation.svg` the SVG optain from the above MEI with [Verovio](https://www.verovio.org)
- `DiSj_n_drum_notation.pdf` the PDF optain from the above SVG with [Inkscape](https://inkscape.org)
- Pour chaque transcription analysée, un répertoire `notes_martin` 

other contents:
- 1 répertoire `0_suggestion_generales`
- `param.ini` a parameter file used by cmd `drumparse`
- dir `schemas/` qparse grammar files for transcription
- dir `scripts/` Python scripts for batch transcription
  see `README.md` in this dir


## beat extraction
frpm GMD audio files, with beatroot Sonic Visualizer plugin.
see https://gitlab.inria.fr/transcription/gmd-transcript

see 

---
former repos (chronological ordering):
- https://gitlab.inria.fr/transcription/gmd-transcript
- https://gitlab.inria.fr/lyrodrig/pyqparse/-/tree/main/tests/groove_transcription/dataset
- https://gitlab.inria.fr/transcription/gmdscores
  public pour WoRMS

## online MIDI visualiser/editor
https://signal.vercel.app/edit



