# D1S1_001 1_funk_80_beat_4-4 


## erreurs nouvelles
beaucoup d'erreurs dans
decembergroove/drum_scores/D1S1_001/1_funk_80_beat_4-4_drum_us.pdf

en particulier par rapport aux versions précédentes.
cf. à la fin du README pour les adresses des précédents repo.

- [https://gitlab.inria.fr/transcription/gmdscores](https://gitlab.inria.fr/transcription/gmdscores)  (WoRMS, en principe les meilleures versions)
  https://gitlab.inria.fr/transcription/gmdscores/-/blob/main/D1S1_001/D1S1_001_drum_agostini.pdf
  https://gitlab.inria.fr/transcription/gmdscores/-/blob/main/D1S1_001/D1S1_001_drum_us.pdf

- https://gitlab.inria.fr/transcription/gmd-transcript (premiers tests)
  https://gitlab.inria.fr/transcription/gmd-transcript/-/blob/main/D1S1_001/1_funk_80_beat_4-4.pdf
  et d'autres pdf sans D1S1_001/ et D1S1_001/case5, case6/
  
- https://gitlab.inria.fr/lyrodrig/pyqparse/-/tree/main/tests/groove_transcription/dataset (Lydia's pyqparse)
- [ ] voir les diff pour D1S1_001 sur l'historique
      https://gitlab.inria.fr/lyrodrig/pyqparse/-/commit/f456452813f81a9ce6433c21f95c0964fe418030#af4752cefdd66601f632e273c5076a86303f92eb

## quelques pistes

### erreurs venant des grammaires et param

- [ ]  regrouper dans DecemberGroove les différents fichiers grammaires des précédents repo. avec différents noms
- [ ]  idem pour param.ini (le paramètre ALPHA est important).
- [ ]  tester dans DecemberGroove avec les différentes grammaires et params


### erreurs venant du code

- [ ] tests unitaires plus en détail `JamDrum.cpp` 
      en étendant `Gtest/JamDrum.cpp`
      https://gitlab.inria.fr/qparse/qparselib/-/blob/adt-groove/test/GTests/TestJamDrum.cpp

- [ ] regarder les commits et comparer les versions suivant les dates des pdf. 


